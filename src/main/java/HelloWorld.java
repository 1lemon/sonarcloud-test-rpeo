public class HelloWorld {

    private static final String PASSWORD_EXAMPLE = "123456789";

    public static void main(String[] args) throws Exception {
        System.out.println("Hello World");

        // Bad code, duplication
        System.out.println("Hello World");

        for (double i = 0.0; i < 0.8; i = Math.random()) {
            System.out.println("Yet another bad loop");
        }

    }
}
